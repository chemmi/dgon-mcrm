# Contributing to this Collection

## Contribution Guidelines for Documents

### Format Guidelines

To ensure consistent format, markdown documents should be in accordance with the following rules.

- copy `_docs/template.md`, rename it to `author_year.md` and add your content
- documents end with a newline
- every sentence is followed by a newline (so max. one sentence per line)
- paragraphs are divided by empty lines
- there are no empty lines in the front matter (i.e. the header of documents)
- the front matter is followed by a single empty line
- the filename of documents is "`<media-id>`.md" (see below how the `media-id` is built)
- comments may be inserted using HTML-like comment style (i.e. `<!-- my comment -->`)
- TODOs my be added using comments (i.e. `<!-- TODO: my todo -->`)

### Minimal Document

- The document must at least contain the `title`, `author`, `year`, `url` and `media-id` fields.
- An _empty abstract_ must be denoted like this:
  ```md
  (Abstract missing)
  <!-- TODO: Abstract missing -->
  ```
- If the abstract of the document is used (e.g. as a placeholder), it has to be followed by a remark like this:
  ```md
  (Abstract taken from original document)
  <!-- TODO: Abstract taken from original document -->
  ```
  
An example for a minimal document named _Meier\_2019.md_ is given below.

```md
---
media-id: Meier_2019
title: Maritime Cyber Security: Practical Guidelines
author: Meier
year: 2019
media-url: https://www.example.com/meier-guidelines.pdf
keywords: key1, key2
---

(Abstract missing)
<!-- TODO: Abstract missing -->
```

### Media-IDs

The field `media-id` is built using the `author` of the document, the `year` in which the document has been published and optionally an incremented letter suffix if the `media-id` is already taken.
It is of the form `<media-id>_<year><opt. letter>`.

Additionally common abbreviations for institutions should be used (e.g. the `media-id` may be "ABS\_2019" although the `author` is _American Bureau of Shipping_)

Examples:
- ABS\_2019, ABS\_2019a, ABS\_2019b, ...
- Meier\_2017, Meier\_2018, Meier\_2018a, ...

### Keywords 

Supply the front matter with some keywords if possible.
Add the keywords at the `keywords:` section within the document header as a comma seperated list.
