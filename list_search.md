---
layout: default
title: "List (search)"
permalink: /list_search/
---
<div>
    <form onkeydown="return event.key != 'Enter';">
        <label class="sr-only" for="search">Enter your search term...</label>
        <input type="search" id="search" tabindex="-1" placeholder="Enter your search term..." />
    </form>
</div>

<table>
  <thead>
    <tr>
      <td> Year </td><td> Title </td><td> Author </td><td> </td> <td> Keywords </td>
    </tr>
  </thead>
  <tbody id="results"></tbody>
</table>

<script src="{{ '/js/jquery.min.js'| relative_url }}"></script>
<script src="{{ '/js/lunr/lunr.min.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-store.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-util.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-search-list.js' | relative_url }}"></script>
<script>
$(document).ready(function() {
    $('input#search').on('keyup', function () {
        var resultstable = $('#results');
        var query = $(this).val().toLowerCase();
        var results = queryResults(listidx, query);
        fillResultsTable(resultstable, results);
    });
});
</script>
