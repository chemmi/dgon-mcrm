---
layout: default
title: "List (full)"
permalink: /list_full/
---

<table>
  <thead>
    <tr>
      <td> Year </td><td> Title </td><td> Author </td><td> </td><td> Keywords </td>
    </tr>
  </thead>
  <tbody>
    {% assign sorted_docs = site.docs | sort: 'year' | reverse %}
    {% for page in sorted_docs %}
    <tr class="media">
      <td> {{ page.year }} </td>
      <td> <a href="{{ page.url | prepend: site.baseurl }}">{{ page.title }}</a> </td>
      <td> {{ page.author }} </td>
      <td>
      {% if page.media-url %}
      <a href="{{ page.media-url }}" title="Link to document"><span class="icon">{% include download.svg %}</span></a>
      {% else %}
      <span class="icon" title="No link to document available">{% include slash.svg %}</span>
      {% endif %}
      </td>
      <td>
      {% if page.keywords %}
      {{ page.keywords }}
      {% endif %}
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>

