---
layout: default
title: "Docs (search)"
permalink: /docs_search/
---

<div class="search-content__inner-wrap">
    <form onkeydown="return event.key != 'Enter';">
        <label class="sr-only" for="search">Enter your search term...</label>
        <input type="search" id="search" tabindex="-1" placeholder="Enter your search term..." />
    </form>
    <div id="results" class="results"></div>
</div>

<script src="{{ '/js/jquery.min.js'| relative_url }}"></script>
<script src="{{ '/js/lunr/lunr.min.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-store.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-util.js' | relative_url }}"></script>
<script src="{{ '/js/lunr/lunr-search.js' | relative_url }}"></script>
<script>
$(document).ready(function() {
    $('input#search').on('keyup', function () {
        var resultsdiv = $('#results');
        var query = $(this).val().toLowerCase();
        var results = queryResults(fullidx, query);
        fillResultsDiv(resultsdiv, results);
    });
});
</script>
