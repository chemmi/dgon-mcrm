---
media-id: ENISA_2019
title: Port Cybersecurity - Good practices for cybersecurity in the maritime sector
author: ENISA
year: 2019
media-url: https://www.enisa.europa.eu/publications/port-cybersecurity-good-practices-for-cybersecurity-in-the-maritime-sector
keywords: recommendation
---

Developed in collaboration with several EU ports, this report intends to provide a useful foundation on which CIOs and CISOs of entities involved in the port ecosystem, especially port authorities and terminal operators, can build their cybersecurity strategy.
The study lists the main threats posing risks to the port ecosystem and describes key cyber-attack scenarios that could impact them.
This approach allowed the identification of security measures that ports shall put in place to better protect themselves from cyberattack.
The main measures identified intend to serve as good practices for people responsible for cybersecurity implementation.
The study can be useful for other stakeholders in the broader community within the port ecosystem, such shipping companies and maritime policy makers. 

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
