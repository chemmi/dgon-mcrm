---
media-id: DNVGL_2016
title: Cyber security resilience management for ships and mobile offshore units in operation (DNVGL-RP-0496)
author: DNV-GL AS
year: 2016
media-url: https://www.dnvgl.com/maritime/dnvgl-rp-0496-recommended-practice-cyber-security-download.html
keywords: recommendation
---

Cyber security has become a concern and should be considered as an integral part of the overall safety management in shipping and offshore operations.
Our recommended practice (RP) explains the ‘how to do’ and not just the ‘what to do’.
We use a structured approach to effectively assess and manage your cyber security by combining IT best practices with an in-depth understanding of maritime operations and industrial automated control systems.
In addition, our RP gives guidance supporting preparations for ISO/IEC 27001 certification.
Get your copy of our DNVGL-RP-0496 on cyber security resilience management for ships and mobile offshore units in operation by submitting the form above.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
