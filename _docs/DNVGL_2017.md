---
media-id: DNVGL_2017
title: Cyber security in the oil and gas industry based on IEC 62443 (DNVGL-RP-G108)
author: DNV-GL
year: 2017
media-url: https://rules.dnvgl.com/docs/pdf/DNVGL/RP/2017-09/DNVGL-RP-G108.pdf
keywords: recommendation
---

Industrial automation and control systems (IACS) in oil and gas installations are vulnerable to cyber security incidents.
As a result, countermeasures must be in place, and the facility operator must be confident that these countermeasures are sufficient and correctly performed.
The risk must be acceptable for all systems including existing and possibly obsolete systems.
Due to the high number of packages and systems in the packages, a standard based approach is required.
Such standards for industrial automation and control systems are evolving, and the IEC 62443 standard is emerging as the preferred approach for many.
The challenges are that this is a generic standard for all industrial components and that it is not finalized yet.
Verifying conformance to the entire set of the IEC 62443 series of standards is very costly, and some parts may be irrelevant to certain industries.
The standard also refers to security levels (SL), but it may be difficult to define the correct target for the different systems.
Currently the standard defines what to do, but does not fully detail how to do it.
To overcome these challenges, this recommended practice for implementing IEC 62443 (3-2, 3-3 and 2-4) in the oil and gas sector was produced based on a joint industry project (JIP) with participation from ABB, DNV GL, Emerson, Honeywell, Kongsberg Maritime, Lundin Norway, Petroleum Safety Authority (PSA) Norway, Shell Norway, Siemens, Statoil and Woodside Energy.
The purpose of this JIP was to define a common and practical approach on how to secure industrial automation and control systems (IACS) in the oil and gas sector.
The recommended practice intends to follow the regulatory requirements defined by PSA for the Norwegian continental shelf and by Health and Safety Executive (HSE) for the UK oil Sector.
