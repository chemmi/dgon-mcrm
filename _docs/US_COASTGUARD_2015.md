---
media-id: US_COASTGUARD_2015
title: Cyber Strategy
author: United States Coast Guard
year: 2015
media-url: https://www.uscg.mil/Portals/0/Strategy/Cyber%20Strategy.pdf
keywords:
---

Cyber technology has fueled great progress and efficiency in our modern world.
Coast Guard operations are more effective because of the rapid evolution in cyber technology, and advanced technologies have also led to an unprecedented era of efficiency of the maritime Transportation System (mTS).
however, with these benefits come serious risks.
information and its supporting systems are continually attacked and exploited by hostile actors.
foreign governments, criminal organizations, and other illicit actors attempt to infiltrate critical government and private sector information systems, representing one of the most serious threats we face as a nation.
