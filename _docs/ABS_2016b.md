---
media-id: ABS_2016b
title: The Application Of Cybersecurity Principles To Marine And Offshore Operations
author: American Bureau of Shipping
year: 2016
media-url: https://cybersail.org/wp-content/uploads/2017/02/ABS-Cyber-Security-Principles-to-Marine-Offshore-Operations.pdf
keywords: guideline,
---

The marine and offshore industries are integrating connected sensors, communications, storage and processing capabilities into vessels, offshore units and facilities as networking and computational power penetrates all aspects of industry operations.
The 'Big Data' phenomenon has emerged as a direct result of this growth, enabling development of tremendous new sources of data and information.
But challenges have also emerged.
Sensors and data must be trustworthy in order to support the new analytic and decision methods available for maritime industry use.
These Guidance Notes are intended to clarify the basic principles and concepts of Data Integrity for marine and offshore assets.
The document is intended to help the industry realize the new benefits from data sources and data analytics systems via implementation of Data Integrity concepts.
It also supports owners who are increasingly required to provide data reporting to regulatory agencies.
The intended users for these Guidance Notes are cybersecurity specialists, data specialists, owners, shipyards, operators, designers, suppliers, review engineers and Surveyors.
