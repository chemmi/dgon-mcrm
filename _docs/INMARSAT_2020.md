---
media-id: INMARSAT_2020
title: Cyber Security Requirements for IMO 2021
author: inmarsat
year: 2020
media-url: https://www2.inmarsat.com/IMO-2021-requirements-whitepaper
keywords:
---

The following report offers ship owners and managers guidance covering their responsibilities under the new IMO regime and explains how the cyber security solution Fleet Secure Endpoint provides a comprehensive tool to support them towards compliance.
