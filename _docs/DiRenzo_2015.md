---
media-id: DiRenzo_2015
title: The Little-known Challenge of Maritime Cyber Security
author: DiRenzo et al.
year: 2015
media-url: https://ieeexplore.ieee.org/document/7388071
keywords:
---

The vulnerabilities to cyber attacks of today's marine transportation system have not been well studied.
This paper explores vulnerabilities of shipboard systems, oil rigs, cargo, and port operations.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
