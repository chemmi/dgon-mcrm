---
media-id: CLASSNK_2019
title: Cyber Security Management System for Ships
author: ClassNK
year: 2019
media-url: https://www.classnk.or.jp/account/de/Rules_Guidance/ssl/guidelines.aspx
keywords: guideline
---

The document provides guidelines to establish, maintain and improve a management system for cyber security to ensure safe ship operation.
The document consists of the two parts <I>Part 1 Requirements</I> and <I>Part 2 Controls</I>.
The first part defines the requirements for a cyber security management system, e.g. the master´s and the operating company´s responsibility or familiarization for personnel.
The second part defines controls to be implemented in the company and onboard to respond to cyber risks during operation, e.g. inventory lists of information communication devices, networks etc. or access control policy.
Also in before the document begins a <I>Cyber Security Approach</I> is provided.
This is meant to provide guidance to estimate if a certain security with respect to the threat of cyber attacks has been achieved.
