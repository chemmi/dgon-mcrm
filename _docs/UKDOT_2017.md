---
media-id: UKDFT_2017
title: Code of Practice – Cyber Security for Ships
author: UK Department for Transport
year: 2017
media-url: https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/642598/cyber-security-code-of-practice-for-ships.pdf
keywords:
---

This Code of Practice should be read by board members of organisations with one or more ships, insurers, ships' senior officers (for example, the Captain/Master, First Officer and Chief Engineer) and those responsible for the day-to-day operation of maritime information technology (IT), operational technology (OT) and communications systems.
It does not set out specific technical or construction standards for ship systems, but instead provides a management framework that can be used to reduce the risk of cyber incidents that could affect the safety or security of the ship, its crew, passengers or cargo.
