---
media-id: DeutscheFlagge_2020
title: ISM Cyber Security
author: Deutsche Flagge
year: 2020
media-url: https://www.deutsche-flagge.de/de/redaktion/dokumente/ism-rundschreiben/circ2020_1_1_eng.pdf
keywords: recommendation
---

The information listed in this document is based on Circular 04/2018 (ISM), has a recommendatory character and describes approaches for creating a Cyber Risk Management System for integration into the company's existing SMS.
The document also shows the in-terfaces to the BSI IT-Grundschutz, the ISPS Code and is intended to provide support for a holistic maritime Cyber Risk Management.
