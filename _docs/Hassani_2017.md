---
media-id: Hassani_2017
title: Cyber Security Issues In Navigation Systems Of Marine Vessels From A Control Perspective
author: Hassani et al.
year: 2017
media-url: https://ntnuopen.ntnu.no/ntnu-xmlui/bitstream/handle/11250/2486072/OMAE2017-61771.pdf
keywords: scientific
---

Autonomous marine vessels are the way forward to revolutionize maritime operations.
However, the safety and success of autonomous missions depend critically on the availability of a reliable positioning system and time information generated using global positioning system (GPS) data.
GPS data are further used for guidance, navigation, and control (GNC) of vehicles.
At a mission planning level GPS data are commonly assumed to be reliable.
From this perspective, this article aims to highlight the perils of maritime navigation attacks, showing the need for the enhancement of standards and security measures to intercept any serious threats to marine vessels emanating from cyber attacks and GPS spoofing.
To this end, we consider a case where a cyber attacker blocks the real GPS signals and dupes the GPS antennas on board the marine vehicle with fake signals.
Using the Nomoto model for the steering dynamics of a marine vesseland exploiting tools from linear control theory we show analytically, and verify using numerical simulations, that it is possible to influence the state variables of the marine vessel by manipulating the compromised GPS data.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
