---
media-id: IEC_2009b
title: IEC 62443-3-1
author: IEC
year: 2009
media-url: https://webstore.iec.ch/publication/7031
keywords: standard
related:
    - IEC_2009a
    - IEC_2010a
---

IEC/TR 62443-3-1:2009(E) provides a current assessment of various cybersecurity tools, mitigation counter-measures, and technologies that may effectively apply to the modern electronically based IACSs regulating and monitoring numerous industries and critical infrastructures.
It describes several categories of control system-centric cybersecurity technologies, the types of products available in those categories, the pros and cons of using those products in the automated IACS environments, relative to the expected threats and known cyber vulnerabilities, and, most important, the preliminary recommendations and guidance for using these cybersecurity technology products and/or countermeasures.
