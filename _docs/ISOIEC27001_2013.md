---
media-id: ISOIEC_2013
title: ISO/IEC27001:2013
author: ISO/IEC
year: 2013
media-url: https://www.iso.org/standard/54534.html
keywords: standard
---

ISO/IEC 27001:2013 specifies the requirements for establishing, implementing, maintaining and continually improving an information security management system within the context of the organization.
It also includes requirements for the assessment and treatment of information security risks tailored to the needs of the organization.
The requirements set out in ISO/IEC 27001:2013 are generic and are intended to be applicable to all organizations, regardless of type, size or nature.
