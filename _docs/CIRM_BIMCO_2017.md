---
media-id: CIRM_BIMCO_2017
title: Industry Standard on Software Maintenance of Shipboard Equipment
author:  CIRM/BIMCO Joint Working Group
year: 2017
media-url: https://www.bimco.org/Industry_standards
keywords: guideline
---

This Industry Standard was developed by a Joint Working Group (“the JWG”) comprising members of Comité International Radio-Maritime (CIRM), the international association of marine electronics companies, and BIMCO, the world’s largest shipping association.
Participants in the work of the JWG included representatives of shipowners, bridge equipment manufacturers, service providers, and system integrators.
A list of companies represented within the JWG can be obtained on request to the Secretariats of CIRM or BIMCO.
The Industry Standard was developed between 2014 and 2017.
The work encompassed a pilot project wherein a draft version of the standard was implemented on board ships on a trial basis, the results of which were used to amend the contents of the standard.
The following companies participated in the pilot project: BP Shipping, Emarat Maritime, Furuno, Kongsberg Maritime, Maersk Line, MAN Diesel & Turbo, Radio Holland, and Sperry Marine.
The Industry Standard was published by CIRM and BIMCO in December 2017.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
