---
media-id: BIMCO_2018
title: The Guidelines on Cyber Security onboard Ships (v3)
author: BIMCO et al.
year: 2018
media-url: https://www.bimco.org/-/media/bimco/about-us-and-our-members/publications/ebooks/cyber-security-guidelines-2018.ashx
keywords: guideline
---

As technology continues to develop, information technology (IT) and operational technology (OT) onboard ships are increasingly being networked together – and more frequently connected to the worldwide web.
This brings the greater risk of unauthorised access or malicious attacks to ships’ systems and networks.
Risks may also occur from personnel having access to the systems onboard, for example by introducing malware via removable media.
Relevant personnel should have training in identifying the typical modus operandi of cyber attacks.
The safety, environmental and commercial consequences of not being prepared for a cyber incident may be significant.
Responding to the increased cyber threat, a group of international shipping organisations, with support from a wide range of stakeholders, have developed these guidelines, which are designed to assist companies develop resilient approaches to cyber security onboard ships.
Approaches to cyber security will be company- and ship-specific, but should be guided by appropriate standards and the requirements of relevant national regulations.
The Guidelines provide a risk-based approach to identifying and responding to cyber threats.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
