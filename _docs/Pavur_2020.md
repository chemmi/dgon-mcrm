---
media-id: Pavur_2020
title: "A Tale of Sea and Sky: On the Security of Maritime VSAT Communications"
author: Pavur et al.
year: 2020
media-url: https://www.cs.ox.ac.uk/publications/publication13778-abstract.html
keywords: scientific
---

Very Small Aperture Terminals (VSAT) have revolutionized maritime operations.
However, the security dimensions of maritime VSAT services are not well understood.
Historically, high equipment costs have acted as a barrier to entry for both researchers and attackers.
In this paper we demonstrate a substantial change in threat model, proving practical attacks against maritime VSAT networks with less than $400 of widely-available television equipment.
This is achieved through GSExtract, a purpose-built forensic tool which enables the extraction of IPtraffic from highly corrupted VSAT data streams.

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
