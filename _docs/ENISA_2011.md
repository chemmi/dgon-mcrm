---
media-id: ENISA_2011
title: Analysis of Cyber Security Aspects in the Maritime Sector
author: ENISA
year: 2011
media-url: https://www.enisa.europa.eu/publications/cyber-security-aspects-in-the-maritime-sector-1
keywords:
---

This report is the first EU report ever on cyber security challenges in the Maritime Sector.
This principal analysis highlights essential key insights, as well as existing initiatives, as a baseline for cyber security.
Finally, high-level recommendations are given for addressing these risks, Cyber threats are a growing menace, spreading to all industry sectors that relying on ICT systems.
Recent deliberate disruptions of critical automation systems, such as Stuxnet, prove that cyber-attacks have a significant impact on critical infrastructures.
Disruption of these ICT capabilities may have disastrous consequences for the EU Member States’ governments and social wellbeing.
The need to ensure ICT robustness against cyber-attacks is thus a key challenge at national and pan-European level. 

(Abstract taken from original document)
<!-- TODO: Abstract taken from original document -->
