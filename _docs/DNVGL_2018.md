---
media-id: DNVGL_2018
title: Cyber security capabilities of control system components (DNVGL-CP-0231)
author: DNV-GL
year: 2018
media-url: https://rules.dnvgl.com/docs/pdf/DNVGL/CP/2018-01/DNVGL-CP-0231.pdf
keywords: guideline
---

DNV GL class programmes contain procedural and technical requirements including acceptance criteria for obtaining and retaining certificates for objects and organisations related to classification.
