---
layout: null
---

var highlight = function (element, matches) {

  var nodeFilter = {
    acceptNode: function (node) {
      if (/^[\t\n\r ]*$/.test(node.nodeValue)) {
        return NodeFilter.FILTER_SKIP
      }
      return NodeFilter.FILTER_ACCEPT
    }
  }

  var index = 0,
      matches = matches.sort(function (a, b) { return a[0] - b[0] }).slice(),
      previousMatch = [-1, -1]
      match = matches.shift(),
      walker = document.createTreeWalker(
        element,
        NodeFilter.SHOW_TEXT,
        nodeFilter,
        false
      )

  while (node = walker.nextNode()) {
    if (match == undefined) break
    if (match[0] == previousMatch[0]) continue

    var text = node.textContent,
        nodeEndIndex = index + node.length;

    if (match[0] < nodeEndIndex) {
      var range = document.createRange(),
          tag = document.createElement('mark'),
          rangeStart = match[0] - index,
          rangeEnd = rangeStart + match[1];

      tag.dataset.rangeStart = rangeStart
      tag.dataset.rangeEnd = rangeEnd

      range.setStart(node, rangeStart)
      range.setEnd(node, rangeEnd)
      range.surroundContents(tag)

      index = match[0] + match[1]

      // the next node will now actually be the text we just wrapped, so
      // we need to skip it
      walker.nextNode()
      previousMatch = match
      match = matches.shift()
    } else {
      index = nodeEndIndex
    }
  }
}


var listidx = lunr(function () {
    this.ref('id')
    this.field('title')
    this.field('author')
    //this.field('abstract')

    this.metadataWhitelist = ['position']

    this.pipeline.remove(lunr.trimmer)
    this.pipeline.remove(lunr.stemmer)

    for (var item in store) {
        this.add({
            title: store[item].title,
            author: store[item].author,
//            abstract: store[item].abstract,
            year: store[item].year,
            id: item
        })
    }
});


var fillResultsTable = function (resultstable, results) {
    resultstable.empty();
//    resultsdiv.prepend('<p>'+results.length+' Result(s) found</p>');
    results.forEach(function (result) {
        resultrow = buildResultRow(result);
        highlightResult(resultrow, result);
        resultstable.append(resultrow);
    })
}

var buildResultRow = function (result) {
    var doc = store[result.ref]
    
    var resultrow = document.createElement('tr')
    resultrow.className = 'media'

    // Year
    var year = document.createElement('td')
    year.className = 'year'
    year.textContent = doc.year
    resultrow.appendChild(year)
    
    // Title with internal url to doc
    var a = document.createElement('a')
    a.href = doc.url
    a.textContent = doc.title
    a.dataset.field = 'title'
    resultrow.appendChild(a)

    // Author
    var author = document.createElement('td')
    author.textContent = doc.author
    author.dataset.field = 'author'
    resultrow.appendChild(author)

    // Media url
    var mediaurl = document.createElement('td')
    if (doc.mediaurl) {
        var icon = document.createElement('span')
        icon.className = 'icon'
        icon.innerHTML = `{% include download.svg %}`
        var urlicon = document.createElement('a')
        urlicon.append(icon)
        urlicon.title = "Link to document"
        urlicon.href = doc.mediaurl
        mediaurl.appendChild(urlicon)
    } else {
        var nolinkicon = document.createElement('span')
        nolinkicon.className = 'icon'
        nolinkicon.innerHTML = `{% include slash.svg %}`
        mediaurl.appendChild(nolinkicon)
    }
    resultrow.append(mediaurl)
    
    // Keywords
    var keywords = document.createElement('td')
    keywords.className = 'keywords'
    if (doc.keywords) {
        keywords.textContent = doc.keywords
    }
    resultrow.appendChild(keywords)

    return resultrow
}
