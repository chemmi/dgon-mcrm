---
layout: null
---

var highlight = function (element, matches) {

  var nodeFilter = {
    acceptNode: function (node) {
      if (/^[\t\n\r ]*$/.test(node.nodeValue)) {
        return NodeFilter.FILTER_SKIP
      }
      return NodeFilter.FILTER_ACCEPT
    }
  }

  var index = 0,
      matches = matches.sort(function (a, b) { return a[0] - b[0] }).slice(),
      previousMatch = [-1, -1]
      match = matches.shift(),
      walker = document.createTreeWalker(
        element,
        NodeFilter.SHOW_TEXT,
        nodeFilter,
        false
      )

  while (node = walker.nextNode()) {
    if (match == undefined) break
    if (match[0] == previousMatch[0]) continue

    var text = node.textContent,
        nodeEndIndex = index + node.length;

    if (match[0] < nodeEndIndex) {
      var range = document.createRange(),
          tag = document.createElement('mark'),
          rangeStart = match[0] - index,
          rangeEnd = rangeStart + match[1];

      tag.dataset.rangeStart = rangeStart
      tag.dataset.rangeEnd = rangeEnd

      range.setStart(node, rangeStart)
      range.setEnd(node, rangeEnd)
      range.surroundContents(tag)

      index = match[0] + match[1]

      // the next node will now actually be the text we just wrapped, so
      // we need to skip it
      walker.nextNode()
      previousMatch = match
      match = matches.shift()
    } else {
      index = nodeEndIndex
    }
  }
}


var queryResults = function (idx, query) {
    var results = idx.query(function (q) {
        query.split(lunr.tokenizer.separator).forEach(function (term) {
            q.term(term, { boost: 100 })
            if(query.lastIndexOf(" ") != query.length-1){
                q.term(term, {  usePipeline: false, wildcard: lunr.Query.wildcard.TRAILING, boost: 10 })
            }
            if (term != ""){
                q.term(term, {  usePipeline: false, editDistance: 1, boost: 1 })
            }
        })
    });
    return results
}


var highlightResult = function (resultobj, result) {
    Object.keys(result.matchData.metadata).forEach(function (term) {
        Object.keys(result.matchData.metadata[term]).forEach(function (fieldName) {
            var field = resultobj.querySelector('[data-field=' + fieldName + ']'),
                positions = result.matchData.metadata[term][fieldName].position
            if (field) {
                highlight(field, positions)
            }
        })
    })
}

