---
layout: null
---

var fullidx = lunr(function () {
    this.ref('id')
    this.field('title')
    this.field('author')
    this.field('abstract')

    this.metadataWhitelist = ['position']

    this.pipeline.remove(lunr.trimmer)
    this.pipeline.remove(lunr.stemmer)

    for (var item in store) {
        this.add({
            title: store[item].title,
            author: store[item].author,
            abstract: store[item].abstract,
            year: store[item].year,
            id: item
        })
    }
});


var fillResultsDiv = function (resultsdiv, results) {
    resultsdiv.empty();
//    resultsdiv.prepend('<p>'+results.length+' Result(s) found</p>');
    results.forEach(function (result) {
        resultdiv = buildResultDiv(result);
        highlightResult(resultdiv, result);
        resultsdiv.append(resultdiv);
    })
}


var buildResultDiv = function (result) {
    var doc = store[result.ref]
    
    var resultdiv = document.createElement('div')
    resultdiv.id = doc.mediaid

    // Title with internal url to doc
    var a = document.createElement('a')
    a.href = doc.url
    var title = document.createElement('media_title')
    title.textContent = doc.title
    title.dataset.field = 'title'
    a.appendChild(title)
    resultdiv.appendChild(a)

    // Author
    var author = document.createElement('author')
    author.textContent = doc.author
    author.dataset.field = 'author'
    resultdiv.appendChild(author)

    // Year
    var year = document.createElement('div')
    year.className = 'year'
    year.textContent = doc.year
    resultdiv.appendChild(year)

    // Media url
    if (doc.mediaurl) {
        var icon = document.createElement('span')
        icon.className = 'icon'
        icon.innerHTML = `{% include download.svg %}`
        var urlicon = document.createElement('a')
        urlicon.append(icon)
        urlicon.title = "Link to document"
        urlicon.href = doc.mediaurl
        resultdiv.appendChild(urlicon)
    } else {
        var nolinkicon = document.createElement('span')
        nolinkicon.className = 'icon'
        nolinkicon.innerHTML = `{% include slash.svg %}`
        resultdiv.appendChild(nolinkicon)
    }
    
    // Abstract
    var abstract_ = document.createElement('abstract')
    abstract_.innerHTML = doc.abstract
    abstract_.dataset.field = 'abstract'
    resultdiv.appendChild(abstract_)

    // Keywords
    var keywords = document.createElement('div')
    keywords.className = 'keywords'
    if (doc.keywords) {
        keywords.textContent = 'Keywords: ' + doc.keywords
    }
    resultdiv.appendChild(keywords)

    return resultdiv
}
