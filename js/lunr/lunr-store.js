---
layout: null
---

var store = [
    {% for page in site.docs %}
        {
            "title": {{ page.title | jsonify }},
            "author": {{ page.author | jsonify }},
            "mediaid": {{ page.media-id | jsonify }},
            "mediaurl": {{ page.media-url | jsonify }},
            "abstract": {{ page.content | strip_html | jsonify }},
            "year": {{ page.year | jsonify }},
            "keywords": {{ page.keywords | jsonify }},
            "url": {{ page.url | prepend: site.baseurl | jsonify }},
        }
    {% unless forloop.last and l %},{% endunless %}
    {% endfor %}
]
