

function filter(query) {
	var mediaList = Array.from(document.getElementsByClassName('media'));
	
	mediaList.forEach( function(elem) {
		var txt = elem.innerText.toString().toLowerCase();
		if (txt.includes(query)) {
			elem.style.display = '';
		} else {
			elem.style.display = 'none';
		}
	});
	
};

function applyFilter(){

	let params = new URLSearchParams(document.location.search.substring(1));
	let query = params.get("search").toLowerCase();

	filter(query);
	
};

function setup() {
	var searchBox = document.getElementById('searchbox');
	
	// handle user input
	searchBox.addEventListener('input', function(){
		var query = searchBox.value;
		history.replaceState(null, null, '?search=' + query);
		applyFilter();
	});
	
	
}

setup();