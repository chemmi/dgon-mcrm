---
layout: default
title: "Docs (full)"
permalink: /docs_full/
---

<ul style="list-style-type:none;">
  {% assign sorted_docs = site.docs | sort: 'year' | reverse %}
  {% for page in sorted_docs %}
  <li class="media">
    <div id="{{ page.media-id }}">
      <a href="{{ page.url | prepend: site.baseurl }}"><media_title>{{ page.title }}</media_title></a>
      <author>{{ page.author }}</author>
      <div class="year">{{ page.year }}</div>
      {% if page.media-url %}
      <a href="{{ page.media-url }}" title="Link to document"><span class="icon">{% include download.svg %}</span></a>
      {% else %}
      <span class="icon" title="No link to document available">{% include slash.svg %}</span>
      {% endif %}
      <abstract>
        {{ page.content }}
      </abstract>
      {% if page.keywords %}
      <div class="keywords">{{ page.keywords }}</div>
      {% endif %}
    </div>
  </li>
  {% endfor %}
</ul>
